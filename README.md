#jquery ui style timepicker plugin#
________________________________________
Easy to intergrate in a form. The timepicker is attached to one ore more input textfields, so the user can select any moment in time , in a format predefined by the developer. Fully customizable, uses jqueryUI css, theme switching possible by jqueryUI themeswitcher
________________________________________
options	default value	description	remark
hour24	0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]	format24 hours , e.g. [0,1,'skip',9,10,11,12,13,14,'-->',18,19,20,21]	string values are shown as disabled buttons
hourAM	[12,1,2,3,4,5,6,7,8,9,10,11]	AM hours	string values are shown as disabled buttons
hourPM	[12,1,2,3,4,5,6,7,8,9,10,11]	PM hours	string values are shown as disabled buttons
hourFormat	12 or 24	AM/PM or 24h format	
hourCols	8	6 or 8	
minDivision	15	specify an array with minutevalues or use one of the built-in formats	array {value1'value2,...valueN} or preformat : number{5,10,12,15,20,30}
inputReadOnly	true or false	disables the inputbox	false : no textinput possible
beforeShow	function(event){}	timepicker-event	event triggered before display timepicker, returns event
onSelect	function(event,complete){}	timepicker-event	event triggerd on hour select : returns event and complete=false, next event triggerd on minute select : returns event and complete=true
onClose	function(event){}	timepicker-event	triggers just before timepicker closes
hourCaption	First choose the whole hour	caption value	string:{caption}
minuteCaption	Then choose the right time	caption value	string:{caption}
button	''	string:{caption} or keyword string {image}, string{default}	caption : displays button with caption, image:displays image clock.png, default:displays jqueryUI clock button
buttonObj	{}	custombutton css	jquery css-object : {'width':'10em'} or e.g. {'background','transparent url(img/myImg.png) no-repeat'}
offsetLeft	0	adjust	finetunes position
offsetTop	0	adjust	finetunes position
closeOnFormclick	false	hides timepicker on external click	triggers on every element without class="ui-..."